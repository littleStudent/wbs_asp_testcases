VU Einfuehrung in Wissensbasierte Systeme
ASP Project: Model-based Diagnosis

Sommersemester 2012
May 19, 2012

Testcases aufgeteilt in component und connector Tests

Eigene Testcases hinzufuegen bitte in eigenem Folder.


----------------------------------------------------


find PATH_TO_REPO/wbs_asp_testcases/modelTests -type f -name "w.test?.dl" -exec echo {} \; -exec dlv -silent -filter=UNCOMPUTED_c,UNCOMPUTED_p,UNEXPECTED_c,UNEXPECTED_p,DUPLICATED_c,DUPLICATED_p -nofacts -N=60 component.tester.dl w.dl {} \;